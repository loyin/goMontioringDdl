module goMontioringDdl

require (
	github.com/CorgiMan/json2 v0.0.0-20150213135156-e72957aba209 // indirect
	github.com/XiaoMi/soar v0.8.2-0.20191023083424-7944a57e1fc8
	github.com/astaxie/beego v1.12.0 // indirect
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548 // indirect
	github.com/dchest/uniuri v0.0.0-20160212164326-8902c56451e9 // indirect
	github.com/gedex/inflector v0.0.0-20170307190818-16278e9db813 // indirect
	github.com/juju/errors v0.0.0-20190930114154-d42613fe1ab9 // indirect
	github.com/percona/go-mysql v0.0.0-20190917053520-301a3504e2a9 // indirect
	github.com/pingcap/errors v0.11.0
	github.com/pingcap/goleveldb v0.0.0-20191031114657-7683883cfb36 // indirect
	github.com/pingcap/log v0.0.0-20191012051959-b742a5d432e9 // indirect
	github.com/pingcap/tidb v2.0.11+incompatible
	github.com/prometheus/client_golang v1.2.1 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20190728182440-6a916e37a237 // indirect
	github.com/russross/blackfriday v2.0.0+incompatible // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644 // indirect
	github.com/shirou/gopsutil v2.19.10+incompatible // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/siddontang/go-log v0.0.0-20190221022429-1e957dd83bed // indirect
	github.com/siddontang/go-mysql v0.0.0-20191019025932-fba9c9655f7c // indirect
	github.com/siddontang/go-mysql-elasticsearch v0.0.0-20190303130753-7b48b8c785d3 // indirect
	github.com/spaolacci/murmur3 v1.1.0 // indirect
	github.com/tidwall/gjson v1.3.4 // indirect
	github.com/uber/jaeger-client-go v2.19.0+incompatible // indirect
	github.com/uber/jaeger-lib v2.2.0+incompatible // indirect
	github.com/youtube/vitess v2.1.1+incompatible // indirect
	github.com/ziutek/mymysql v1.5.4 // indirect
	gopkg.in/yaml.v2 v2.2.5 // indirect
	julive.com/river v1.2.3
	vitess.io/vitess v0.0.0-20191106000153-8de7237f7b75
)

replace (
	cloud.google.com/go => github.com/googleapis/google-cloud-go v0.47.1-0.20191105204433-08bca813144c
	cloud.google.com/go/bigquery => github.com/googleapis/google-cloud-go/bigquery v1.2.1-0.20191105204433-08bca813144c
	cloud.google.com/go/datastore => github.com/googleapis/google-cloud-go/datastore v1.0.1-0.20191105204433-08bca813144c
	cloud.google.com/go/pubsub => github.com/googleapis/google-cloud-go/pubsub v1.0.2-0.20191105204433-08bca813144c
	cloud.google.com/go/storage => github.com/googleapis/google-cloud-go/storage v1.2.0
	github.com/pingcap/tidb/parser => github.com/pingcap/parser v0.0.0-20190506092653-e336082eb825
	go.etcd.io/etcd => github.com/etcd-io/etcd v0.5.0-alpha.5.0.20190320044326-77d4b742cdbf
	go.opencensus.io => github.com/census-instrumentation/opencensus-go v0.22.2-0.20191015192041-3b5a343282fe
	golang.org/x/crypto => github.com/golang/crypto v0.0.0-20191105034135-c7e5f84aec59
	golang.org/x/exp => github.com/golang/exp v0.0.0-20191030013958-a1ab85dbe136
	golang.org/x/image => github.com/golang/image v0.0.0-20191009234506-e7c1f5e7dbb8
	golang.org/x/lint => github.com/golang/lint v0.0.0-20190930215403-16217165b5de
	golang.org/x/mobile => github.com/golang/mobile v0.0.0-20191031020345-0945064e013a
	golang.org/x/mod => github.com/golang/mod v0.1.1-0.20191105210325-c90efee705ee
	golang.org/x/net => github.com/golang/net v0.0.0-20191105084925-a882066a44e0
	golang.org/x/oauth2 => github.com/golang/oauth2 v0.0.0-20190604053449-0f29369cfe45
	golang.org/x/sync => github.com/golang/sync v0.0.0-20190911185100-cd5d95a43a6e
	golang.org/x/sys => github.com/golang/sys v0.0.0-20191028164358-195ce5e7f934
	golang.org/x/text => github.com/golang/text v0.3.0
	golang.org/x/time => github.com/golang/time v0.0.0-20191024005414-555d28b269f0
	golang.org/x/tools => github.com/golang/tools v0.0.0-20191105231337-689d0f08e67a
	golang.org/x/xerrors => github.com/golang/xerrors v0.0.0-20191011141410-1b5146add898
	google.golang.org/api => github.com/googleapis/google-api-go-client v0.13.1-0.20191105000111-473217c7f590
	google.golang.org/appengine => github.com/golang/appengine v1.6.6-0.20191016204603-16bce7d3dc4e
	google.golang.org/genproto => github.com/googleapis/go-genproto v0.0.0-20191028173616-919d9bdd9fe6
	google.golang.org/grpc => github.com/grpc/grpc-go v1.2.1-0.20191104222216-583401aac5f1
	julive.com/river => ./river
	vitess.io/vitess => github.com/vitessio/vitess v0.0.0-20191106000153-8de7237f7b75
)

go 1.13
